var danhSachSinhVien = new DanhSachSinhVien();
// GetStorage(); //Khi load lại web thì tự động lấy dữ liệu đã lưu từ storage lên luôn
var validate = new Validation();

//Bổ sung thuộc tính
SinhVien.prototype.DiemToan = "";
SinhVien.prototype.DiemLy = "";
SinhVien.prototype.DiemHoa = "";
SinhVien.prototype.DTB = "";
SinhVien.prototype.Loai = "";

//Bổ sung phương thức
SinhVien.prototype.TinhDTB = function(){
    this.DTB = ((Number(this.DiemToan) + Number(this.DiemLy) + Number(this.DiemHoa))/3).toFixed(2);
}
SinhVien.prototype.XepLoai = function(){
    if(this.DTB <= 10 && this.DTB >= 8){
        this.Loai = "Xếp loại giỏi";
    }
    else if(this.DTB < 8 && this.DTB >= 6.5){
        this.Loai = "Xếp loại khá";
    }
    else if(this.DTB < 6.5 && this.DTB >= 5){
        this.Loai = "Xếp loại trung bình";
    }
    else{
        this.Loai = "Xếp loại yếu";
    } 
}

function DomID(id){
    var element = document.getElementById(id);
    return element;
}

function ThemSinhVien(){
    //Lấy dữ liệu từ người dùng nhập vào
    var masv = DomID("masv").value;
    var hoten = DomID("hoten").value;
    var cmnd = DomID("cmnd").value;
    var email = DomID("email").value;
    var sdt = DomID("sdt").value;
    var diemtoan = DomID("Toan").value;
    var diemly = DomID("Ly").value;
    var diemhoa = DomID("Hoa").value;
    var error = 0;
    //KIỂM TRA VALIDATION
    // if(validate.KiemTraRong(masv) == true){
    //     DomID("masv").style.borderColor = "red";
    // }
    // else{
    //     DomID("masv").style.borderColor = "green";
    // }
    if(KiemTraDauVaoRong("masv",masv) == true){
        error++;
    }
    if(KiemTraDauVaoRong("hoten",hoten) == true){
        error++;
    }
    if(KiemTraDauVaoRong("cmnd",cmnd) == true){
        error++;
    }
    // if(KiemTraDauVaoRong("email",email) == true){
    //     error++;
    // }
    if(validate.KiemTraEmail(email)){
        DomID("email").style.borderColor = "green";
    }
    else{
        DomID("email").style.borderColor = "red";
        error++;
    }
    if(KiemTraDauVaoRong("sdt",sdt) == true){
        error++;
    }
    if(validate.KiemTraSoDTVietNam(sdt)){
        DomID("sdt").style.borderColor = "green";
    }
    else{
        DomID("sdt").style.borderColor = "red";
        error++;
    }
    // if(KiemTraDauVaoRong("Toan",diemtoan) == true){
    //     error++;
    // }
    // if(KiemTraDiemNhapVao("Toan",diemtoan) == true){
    //     error++;
    // }
    if(error != 0){
        return;
    }
    //THÊM SINH VIÊN
    var sinhvien = new SinhVien(masv, hoten, email, sdt, cmnd);
    sinhvien.DiemToan = DomID("Toan").value;
    sinhvien.DiemLy = DomID("Ly").value;
    sinhvien.DiemHoa = DomID("Hoa").value;
    sinhvien.TinhDTB();
    sinhvien.XepLoai();
    danhSachSinhVien.ThemSinhVien(sinhvien);
    CapNhatDanhSachSV(danhSachSinhVien);
    console.log(danhSachSinhVien);
    // danhSachSinhVien.SuaSinhVien(sinhvien);
    // CapNhatDanhSachSV(danhSachSinhVien);
}

function KiemTraDauVaoRong(ID, value){
    //Kiểm tra mã sinh viên rỗng
    if(validate.KiemTraRong(value) == true){
        DomID(ID).style.borderColor  = "red";
        return true;
    }
    else{
        DomID(ID).style.borderColor = "green";
        return false;
    }
}

// function KiemTraDiemNhapVao(ID, value){
//     if(validate.KiemTraGiaTri(value) == true){
//         DomID(ID).style.borderColor = "green";
//         return true;
//     }
//     else{
//         DomID(ID).style.borderColor =  "red";
//         return false;
//     }
// }

function CapNhatDanhSachSV(DanhSachSinhVien){
    var listTableSV = DomID("tbodySinhVien");
    listTableSV.innerHTML = "";
    for(var i = 0; i < DanhSachSinhVien.DSSV.length; i++){
        //Lấy thông tin sinh viên từ mảng sinh viên
        var sv = DanhSachSinhVien.DSSV[i];
        //Tạo thẻ tr
        var trSinhVien = document.createElement("tr");
        trSinhVien.id = sv.MaSV;
        trSinhVien.className = "trSinhVien";
        trSinhVien.setAttribute("onclick", "ChinhSuaSinhVien('"+sv.MaSV+"')");
        //Tạo các thẻ td và filter dữ liệu sinh viên thứ [i] vào
        var tdCheckBox = document.createElement("td");
        var ckbMaSinhVien = document.createElement("input");
        console.log(ckbMaSinhVien);
        ckbMaSinhVien.setAttribute("class","ckbMaSV");
        ckbMaSinhVien.setAttribute("type","checkbox");
        ckbMaSinhVien.setAttribute("value",sv.MaSV);
        tdCheckBox.appendChild(ckbMaSinhVien);

        var tdMaSV = TaoTheTD("MaSV",sv.MaSV);
        var tdHoTen = TaoTheTD("HoTen", sv.HoTen);
        var tdEmail = TaoTheTD("Email", sv.Email);
        var tdCMND = TaoTheTD("CMND", sv.CMND);
        var tdSoDT = TaoTheTD("SoDT", sv.SoDT);

        //Tạo td DTB và Xếp loại
        var tdDTB = TaoTheTD("DTB", sv.DTB);
        var tdXepLoai = TaoTheTD("XepLoai", sv.Loai);
        //Append cái td vào tr
        trSinhVien.appendChild(tdCheckBox);
        trSinhVien.appendChild(tdMaSV);
        trSinhVien.appendChild(tdHoTen);
        trSinhVien.appendChild(tdEmail);
        trSinhVien.appendChild(tdCMND);
        trSinhVien.appendChild(tdSoDT);
        trSinhVien.appendChild(tdDTB);
        trSinhVien.appendChild(tdXepLoai);
        //Append các tr vào tbodySinhVien
        listTableSV.appendChild(trSinhVien);
    }
}

function TaoTheTD(thuoctinh, value){
    var td = document.createElement("td");
    // td.className = thuoctinh;
    td.innerHTML = value;
    return td;
}

function SetStorage(){
    //chuyển đổi object mảng danh sách sinh viên thành chuỗi json
    var jsonDanhSachSinhVien = JSON.stringify(danhSachSinhVien.DSSV);
    //rồi đem chỗi json lưu vào storage và đặt tên là DanhSachSV
    localStorage.setItem("DanhSachSV",jsonDanhSachSinhVien);
}

function GetStorage(){
    //lấy ra chuỗi json là mảng danh sách sinh viên thông qua tên DanhSachSV
    var jsonDanhSachSinhVien = localStorage.getItem("DanhSachSV");
    var mangDSSV = JSON.parse(jsonDanhSachSinhVien);
    danhSachSinhVien.DSSV = mangDSSV;
    CapNhatDanhSachSV(danhSachSinhVien);
}

function XoaSinhVien(){
    //Mảng checkbox
    var listMaSV = document.getElementsByClassName("ckbMaSV");
    //Mảng mã sinh viên được chọn
    var listMaSVDuocChon = [];
    for(var i=0; i<listMaSV.length; i++){
        console.log(listMaSV[i]);
        if(listMaSV[i].checked){ //Kiểm tra phần tử checkbox đó có được chọn hay chưa
            listMaSVDuocChon.push(listMaSV[i].value);
        }
    } 
    danhSachSinhVien.XoaSinhVien(listMaSVDuocChon);
    CapNhatDanhSachSV(danhSachSinhVien);
}

function TimKiemSinhVien(){
    var tukhoa = DomID("tukhoa").value;
    var listDanhSachSinhVienTimKiem = danhSachSinhVien.TimKiemSinhVien(tukhoa);
    CapNhatDanhSachSV(listDanhSachSinhVienTimKiem);
}

function ChinhSuaSinhVien(masv){
    var sinhvien = danhSachSinhVien.TimSVTheoMa(masv);
    if(sinhvien != null){
        DomID("masv").value = sinhvien.MaSV;
        DomID("hoten").value = sinhvien.HoTen;
        DomID("email").value = sinhvien.Email;
        DomID("cmnd").value = sinhvien.CMND;
        DomID("sdt").value = sinhvien.SoDT;
    }
}

function LuuCapNhatSV(){
    //Lấy dữ liệu từ người dùng nhập vào
    var masv = DomID("masv").value;
    var hoten = DomID("hoten").value;
    var cmnd = DomID("cmnd").value;
    var email = DomID("email").value;
    var sdt = DomID("sdt").value;
    var error = 0;
    //KIỂM TRA VALIDATION
    // if(validate.KiemTraRong(masv) == true){
    //     DomID("masv").style.borderColor = "red";
    // }
    // else{
    //     DomID("masv").style.borderColor = "green";
    // }
    if(KiemTraDauVaoRong("masv",masv) == true){
        error++;
    }
    if(KiemTraDauVaoRong("hoten",hoten) == true){
        error++;
    }
    if(KiemTraDauVaoRong("cmnd",cmnd) == true){
        error++;
    }
    // if(KiemTraDauVaoRong("email",email) == true){
    //     error++;
    // }
    if(validate.KiemTraSoDTVietNam(sdt)){
        error++;
    }
    if(KiemTraDauVaoRong("sdt",sdt) == true){
        error++;
    }
    if(validate.KiemTraEmail(email)){
        DomID("email").style.borderColor = "green";
    }
    else{
        DomID("email").style.borderColor = "red";
        error++;
    }
    if(error != 0){
        return;
    }
    //THÊM SINH VIÊN
    var sinhvien = new SinhVien(masv, hoten, email, sdt, cmnd);
    // danhSachSinhVien.ThemSinhVien(sinhVien);
    // CapNhatDanhSachSV(danhSachSinhVien);
    // console.log(danhSachSinhVien);
    sinhvien.DiemToan = DomID("Toan").value;
    sinhvien.DiemLy = DomID("Ly").value;
    sinhvien.DiemHoa = DomID("Hoa").value;
    sinhvien.TinhDTB();
    sinhvien.XepLoai();
    danhSachSinhVien.SuaSinhVien(sinhvien);
    CapNhatDanhSachSV(danhSachSinhVien);
}