function DanhSachSinhVien(){
    this.DSSV = [];
    this.ThemSinhVien = function(svThem){
        this.DSSV.push(svThem);
    }
    this.XoaSinhVien = function(listSVXoa){
        for(var i=0; i<listSVXoa.length; i++){
            for(var j=0; j<this.DSSV.length; j++){
                var sinhvien = this.DSSV[j];
                if(listSVXoa[i] == sinhvien.MaSV){
                    this.DSSV.splice(j,1);
                }
            }
        }
    }
    this.SuaSinhVien = function(svCapNhap){
        for(var i=0; i<this.DSSV.length; i++){
            var svUpdate = this.DSSV[i];
            if(svCapNhap.MaSV == svUpdate.MaSV){
                svUpdate.HoTen = svCapNhap.HoTen;
                svUpdate.Email = svCapNhap.Email;
                svUpdate.CMND = svCapNhap.CMND;
                svUpdate.SoDT = svCapNhap.SoDT;
            }
        }
    }
    this.TimKiemSinhVien = function(keyword){
        //List kết quả tìm kiếm: kiểu dữ liệu là DanhSachSinhVien
        var listKetQuaTimKiem = new DanhSachSinhVien();
        for(var i=0; i<this.DSSV.length; i++){
            var sinhvien = this.DSSV[i];
            if(sinhvien.HoTen.toLowerCase().trim().search(keyword.toLowerCase().trim()) != -1){
                listKetQuaTimKiem.ThemSinhVien(sinhvien);
            }
        }
        return listKetQuaTimKiem;
    }
    this.TimSVTheoMa = function(masv){
        for(var i=0; i<this.DSSV.length; i++){
            var sv = this.DSSV[i];
            if(sv.MaSV === masv){
                return sv;
            }
        }
        return null;
    }
}