function Validation(){
    this.KiemTraRong = function(value){
        if(value.trim() === ""){
            return true;
        }
        return false;
    }
    this.KiemTraEmail = function(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    this.KiemTraSoDTVietNam =  function(number) {
        const isNumber = /(03|07|08|09|01[2|6|8|9])+([0-9]{8})\b/;
        return isNumber.test(number);
    }
    // this.KiemTraGiaTri = function(value){
    //     if(Number(value) >= 10 || Number(value) < 0){
    //         return false;
    //     }
    //     return true;
    // }
}